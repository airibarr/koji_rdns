#!/bin/bash

TEST=${1:-koji moshimoshi}

export KRB5_TRACE=/dev/stdout

klist 2>/dev/null 1>&2
if [[ $? -ne 0 ]]; then
  echo "You must run kinit first"
  exit
fi

echo "Current config of rdns:"
grep -R rdns /etc/krb5.conf*
echo "Current config of dns_canonical_hostname:"
grep -R dns_canonical_hostname /etc/krb5.conf*

c=0
while [[ $c -le 20 ]]; do
  echo
  $TEST
  if [[ $? -eq 0 ]]; then
    echo -e "\e[1m\e[32mWorked\e[0m"
  else
    echo -e "\e[1m\e[31mFailed!!\e[0m"
    exit
  fi
  c=$(($c+1))
done

echo
echo "Worked enough times, it's probably fine"

