# Docker image to test Koji vs. Kerberos

0. Run this docker image (at CERN (aiadm8/lxplus8), koji is not accessible from the outside):
    ```bash
    [aiadm10] ~ > podman --root /tmp/${USER}/containers run -it gitlab-registry.cern.ch/airibarr/koji_rdns/koji_rdns:latest
    ```
1. Run kinit with your user, provided you have access to Koji.
2. Run `./test.sh` and observe results. You can also run `./test.sh /root/simple.py` to run a simple python reproducer, without using the Koji client.
3. Edit `krb5.conf` and repeat #2.

