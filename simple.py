#!/usr/bin/python3

import sys
import requests
import requests_gssapi

HUB = "kojihub.cern.ch"

r = requests.post(
    "https://{}/kojihub/ssllogin".format(HUB),
    auth=requests_gssapi.HTTPSPNEGOAuth(),
    verify=False)

if r.status_code == requests.codes.ok:
    print('Login successful!')
else:
    print('Authentication failed')
    sys.exit(1)

