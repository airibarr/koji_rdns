FROM gitlab-registry.cern.ch/linuxsupport/c8-base:latest

COPY koji-1.24.0-3.el8.noarch.rpm python3-koji-1.24.0-3.el8.noarch.rpm /tmp/

RUN dnf install -y epel-release \
  && dnf install -y krb5-workstation python3-requests-gssapi \
  && dnf install -y /tmp/koji-1.24.0-3.el8.noarch.rpm /tmp/python3-koji-1.24.0-3.el8.noarch.rpm \
  && dnf clean all

COPY koji.conf /etc/
COPY test.sh /root
COPY simple.py /root

WORKDIR /root
